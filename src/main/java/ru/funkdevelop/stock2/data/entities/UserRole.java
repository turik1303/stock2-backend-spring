package ru.funkdevelop.stock2.data.entities;

import javax.persistence.*;

@Entity
@Table(name = "userroles", schema = "public")
public class UserRole {
    @Id
    @SequenceGenerator(name = "userRole_id_seq",
            sequenceName = "userRole_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "userRole_id_seq")
    @Column(name = "id")
    private Long id;
    @Column(name = "role")
    private String role;
    @Column(name = "roleName")
    private String roleName;


    public UserRole() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
