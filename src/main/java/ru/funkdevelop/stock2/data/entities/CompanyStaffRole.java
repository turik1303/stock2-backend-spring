package ru.funkdevelop.stock2.data.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "company_roles", schema = "public")
public class CompanyStaffRole implements Serializable {
    private static final long serialVersionUID = 1681182382236322985L;

    @Id
    @SequenceGenerator(name = "company_roles_id_seq",
            sequenceName = "company_roles_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "company_roles_id_seq")
    @Column(name = "id")
    private Long id;
    @Column(name = "role")
    private String role;
    @Column(name = "role_name")
    private String roleName;

    public CompanyStaffRole() {
    }

    public CompanyStaffRole(String role, String roleName) {
        this.role = role;
        this.roleName = roleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
