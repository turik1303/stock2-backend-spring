package ru.funkdevelop.stock2.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "companies", schema = "public")
public class Company implements Serializable {
    private static final long serialVersionUID = 1681182382236322985L;

    @Id
    @SequenceGenerator(name = "company_id_seq",
            sequenceName = "company_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "company_id_seq")
    @Column(name = "id")
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")
    @JsonIgnore
    private User owner;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "detailsId", nullable = true)
    private CompanyDetails companyDetails;

    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
    private List<CompanyStaff> staff;

    public Company() {
    }

    public Company(User owner, CompanyDetails companyDetails, List<CompanyStaff> staff) {
        this.owner = owner;
        this.companyDetails = companyDetails;
        this.staff = staff;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public CompanyDetails getCompanyDetails() {
        return companyDetails;
    }

    public void setCompanyDetails(CompanyDetails companyDetails) {
        this.companyDetails = companyDetails;
    }

    public List<CompanyStaff> getStaff() {
        return staff;
    }

    public void setStaff(List<CompanyStaff> staff) {
        this.staff = staff;
    }
}
