package ru.funkdevelop.stock2.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "userDetails")
public class UserDetails {
    private static final long serialVersionUID = 1681182382236322985L;
    @Id
    @SequenceGenerator(name = "stockUserDetails_id_seq",
            sequenceName = "stockUserDetails_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "stockUserDetails_id_seq")
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "patronymic")
    private String patronymic;
    @Column(name = "surname")
    private String surname;
    @Column(name = "birthDay")
    private Date birthDay;
    @JsonIgnore
    @OneToOne(mappedBy = "userDetails")
    private User user;

    public UserDetails(String name, String patronymic, String surname, Date birthDay, User user) {
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
        this.birthDay = birthDay;
        this.user = user;
    }

    public UserDetails() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
