package ru.funkdevelop.stock2.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "company_staff", schema = "public")
public class CompanyStaff implements Serializable {
    private static final long serialVersionUID = 1681182382236322985L;

    @Id
    @SequenceGenerator(name = "company_staff_id_seq",
            sequenceName = "company_staff_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "company_staff_id_seq")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    @JsonIgnore
    private Company company;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "user_id", nullable = false)
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "company_staff_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<CompanyStaffRole> roles;

    public CompanyStaff() {
    }

    public CompanyStaff(Company company, Long userId, List<CompanyStaffRole> roles) {
        this.company = company;
        this.userId = userId;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<CompanyStaffRole> getRoles() {
        return roles;
    }

    public void setRoles(List<CompanyStaffRole> roles) {
        this.roles = roles;
    }
}
