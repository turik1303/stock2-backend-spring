package ru.funkdevelop.stock2.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "company_details", schema = "public")
public class CompanyDetails implements Serializable {
    private static final long serialVersionUID = 1681182382236322985L;

    @Id
    @SequenceGenerator(name = "company_details_id_seq",
            sequenceName = "company_details_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "company_details_id_seq")
    @Column(name = "id")
    private Long id;
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "physical_address", nullable = true)
    private String physicalAddress;
    @Column(name = "legal_address", nullable = true)
    private String legalAddress;
    @JsonIgnore
    @OneToOne(mappedBy = "companyDetails")
    private Company company;

    public CompanyDetails() {
    }

    public CompanyDetails(String name, String physicalAddress, String legalAddress, Company company) {
        this.name = name;
        this.physicalAddress = physicalAddress;
        this.legalAddress = legalAddress;
        this.company = company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(String legalAddress) {
        this.legalAddress = legalAddress;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
