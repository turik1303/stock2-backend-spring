package ru.funkdevelop.stock2.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "stockusers", schema = "public")

public class User implements Serializable {
    private static final long serialVersionUID = 1681182382236322985L;
    @Id
    @SequenceGenerator(name = "stockUser_id_seq",
            sequenceName = "stockUser_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "stockUser_id_seq")
    @Column(name = "id")
    private Long id;
    @Column(name = "userLogin", unique = true)
    private String userLogin;
    @Column(name = "userEmail", unique = true)
    private String userEmail;
    @Column(name = "userPassword")
    private String userPassword;
    @Column(name = "isActive")
    private boolean active;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_role", joinColumns
            = @JoinColumn(name = "user_id",
            referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",
                    referencedColumnName = "id"))
    private List<UserRole> userRoles;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private List<Company> companies;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "detailsId", nullable = true)
    private UserDetails userDetails;

    public User() {
    }

    public User(String userLogin, String userEmail, String userPassword, boolean isActive, List<UserRole> userRoles, UserDetails userDetails, List<Company> companies) {
        this.userLogin = userLogin;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.active = isActive;
        this.userRoles = userRoles;
        this.userDetails = userDetails;
        this.companies = companies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }
}
