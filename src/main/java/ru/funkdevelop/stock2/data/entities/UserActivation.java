package ru.funkdevelop.stock2.data.entities;

import javax.persistence.*;

@Entity
@Table(name = "userActivations", schema = "public")
public class UserActivation {
    private static final long serialVersionUID = 1681182382236322985L;
    @Id
    @SequenceGenerator(name = "stockUserActivation_id_seq",
            sequenceName = "stockUserActivation_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "stockUserActivation_id_seq")
    @Column(name = "id")
    private Long id;
    @Column(name = "userId")
    private Long userId;
    @Column(name = "activationCode")
    private String activationCode;

    public UserActivation() {
    }

    public UserActivation(Long userId, String activationCode) {
        this.userId = userId;
        this.activationCode = activationCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }
}
