package ru.funkdevelop.stock2.data.DAO.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdevelop.stock2.data.entities.CompanyStaffRole;

import java.util.Optional;

public interface CompanyRoleRepository extends JpaRepository<CompanyStaffRole, Long> {
    @Override
    Optional<CompanyStaffRole> findById(Long aLong);
}
