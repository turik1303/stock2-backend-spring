package ru.funkdevelop.stock2.data.DAO.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdevelop.stock2.data.entities.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUserLogin(String userlogin);

    User findByUserEmail(String userEmail);

    Optional<User> findById(Long userId);

    User findByUserLoginAndUserEmail(String userLogin, String userEmail);
}
