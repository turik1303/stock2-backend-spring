package ru.funkdevelop.stock2.data.DAO.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdevelop.stock2.data.entities.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
}
