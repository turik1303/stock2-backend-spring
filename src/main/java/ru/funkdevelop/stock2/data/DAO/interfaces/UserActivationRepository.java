package ru.funkdevelop.stock2.data.DAO.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdevelop.stock2.data.entities.UserActivation;

public interface UserActivationRepository extends JpaRepository<UserActivation, Long> {
    UserActivation findByActivationCode(String activationCode);
}
