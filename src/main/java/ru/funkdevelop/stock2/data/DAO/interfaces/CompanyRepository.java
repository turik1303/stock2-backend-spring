package ru.funkdevelop.stock2.data.DAO.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.funkdevelop.stock2.data.entities.Company;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Long> {

    Optional<Company> findById(Long id);
}
