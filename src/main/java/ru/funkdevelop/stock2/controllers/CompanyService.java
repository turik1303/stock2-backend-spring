package ru.funkdevelop.stock2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.funkdevelop.stock2.data.DAO.interfaces.CompanyRepository;
import ru.funkdevelop.stock2.data.entities.Company;
import ru.funkdevelop.stock2.exceptions.CompanyNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    CompanyRepository companyRepository;


    public Company create(Company company) {
        return null;
    }

    public Company update(Company company) {
        return null;
    }

    public boolean delete(Company company) {
        return false;
    }

    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    public Company getCompanyById(Long id) throws CompanyNotFoundException {
        Optional<Company> company = companyRepository.findById(id);
        if (company.isPresent()) {
            return company.get();
        } else {
            throw new CompanyNotFoundException("Company " + id + "not found!");
        }
    }
}
