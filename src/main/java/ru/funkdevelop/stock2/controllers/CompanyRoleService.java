package ru.funkdevelop.stock2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import ru.funkdevelop.stock2.data.DAO.interfaces.CompanyRoleRepository;
import ru.funkdevelop.stock2.data.entities.CompanyStaffRole;
import ru.funkdevelop.stock2.exceptions.CompanyRoleNotFoundException;

import java.util.List;
import java.util.Optional;

public class CompanyRoleService {
    @Autowired
    private CompanyRoleRepository companyRoleRepository;

    public List<CompanyStaffRole> getAllRoles() {
        return companyRoleRepository.findAll();
    }

    public CompanyStaffRole getRoleById(Long id) throws CompanyRoleNotFoundException {

        Optional<CompanyStaffRole> role = companyRoleRepository.findById(id);
        if (role.isPresent()) {
            return role.get();
        } else {
            throw new CompanyRoleNotFoundException("Role " + id + "not found!");
        }
    }

    public CompanyStaffRole create(CompanyStaffRole role) {
        companyRoleRepository.saveAndFlush(role);
        return role;
    }

    public CompanyStaffRole update(CompanyStaffRole role) {
        companyRoleRepository.saveAndFlush(role);
        return role;
    }

    public Boolean delete(CompanyStaffRole role) {
        try {
            companyRoleRepository.delete(role);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
