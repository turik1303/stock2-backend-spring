package ru.funkdevelop.stock2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.funkdevelop.stock2.data.DAO.interfaces.UserActivationRepository;
import ru.funkdevelop.stock2.data.DAO.interfaces.UserRepository;
import ru.funkdevelop.stock2.data.DAO.interfaces.UserRoleRepository;
import ru.funkdevelop.stock2.data.entities.User;
import ru.funkdevelop.stock2.data.entities.UserActivation;
import ru.funkdevelop.stock2.data.entities.UserDetails;
import ru.funkdevelop.stock2.data.entities.UserRole;
import ru.funkdevelop.stock2.exceptions.UserActivationNotFoundException;
import ru.funkdevelop.stock2.exceptions.UserNotFoundException;
import ru.funkdevelop.stock2.exceptions.UserRegisterException;
import ru.funkdevelop.stock2.services.internal.MailHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserActivationRepository userActivationRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private MailHelper mailHelper;
    @Autowired
    private PasswordEncoder passwordEncoder;


    public boolean checkLogin(String userLogin) {
        User user = userRepository.findByUserLogin(userLogin);
        if (user != null) {
            return true;
        } else {
            return false;
        }
    }


    public boolean checkEmail(String userEmail) {
        User user = userRepository.findByUserEmail(userEmail);
        if (user != null) {
            return true;
        } else {
            return false;
        }
    }


    public User register(User user) throws UserRegisterException {
        try {
            Optional<UserRole> defaultRole = userRoleRepository.findById(2L);
            UserRole role;
            if (defaultRole.isPresent()) {
                role = defaultRole.get();
                List<UserRole> roles = new ArrayList<>();
                roles.add(role);
                user.setUserRoles(roles);
            }
            user.setUserPassword(passwordEncoder.encode(user.getUserPassword()));
            user.setActive(false);
            userRepository.saveAndFlush(user);
            String activationCode = generateRandomString(16);
            UserActivation activation = new UserActivation(user.getId(), activationCode);
            userActivationRepository.saveAndFlush(activation);
            mailHelper.sendMail(user.getUserEmail(), "Активация профиля Stock2", "Ваш код активации: " + activation.getActivationCode());
        } catch (Exception e) {
            throw new UserRegisterException("Ошибка регистрации пользователя" + user.getUserLogin());
        }
        return user;
    }


    public User getUser(Long userId) throws UserNotFoundException {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new UserNotFoundException("Пользователь с Id = " + userId.toString() + "не найден");
        }
    }


    public Boolean refreshPassword(String userLogin, String userEmail) throws UserNotFoundException {
        User user = userRepository.findByUserLoginAndUserEmail(userLogin, userEmail);
        if (user == null) {
            throw new UserNotFoundException("Пользователь с логином " + userLogin + " и Email'ом " + userEmail + " не найден");
        } else {
            String tmpPassword = generateRandomString(8);
            user.setUserPassword(passwordEncoder.encode(tmpPassword));
            userRepository.saveAndFlush(user);
            mailHelper.sendMail(user.getUserEmail(), "Восствновление пароля к Stock2", "Ваш временный пароль: " + tmpPassword);
            return true;
        }
    }


    public boolean activate(String activationCode) throws UserNotFoundException, UserActivationNotFoundException {
        UserActivation activation = userActivationRepository.findByActivationCode(activationCode);
        if (activation != null) {
            Optional<User> user = userRepository.findById(activation.getUserId());
            if (user.isPresent()) {
                user.get().setActive(true);
                userRepository.saveAndFlush(user.get());
                return true;
            } else {
                throw new UserNotFoundException("Пользователь с кодом активации " + activationCode + " не найден");
            }
        } else {
            throw new UserActivationNotFoundException("Ошибка активации пользователя с Id " + activation.getUserId().toString());
        }
    }


    public User getUserByUserName(String userName) throws UserNotFoundException {
        try {
            return userRepository.findByUserLogin(userName);
        } catch (Exception e) {
            throw new UserNotFoundException("Пользователь с логином " + userName + "не найден");
        }
    }

    public User updateUser(User userForUpdate) {
        Optional<User> oldUser = userRepository.findById(userForUpdate.getId());
        User user = oldUser.get();
        if (!userForUpdate.getUserPassword().equalsIgnoreCase("[hidden]")) {
            user.setUserPassword(passwordEncoder.encode(userForUpdate.getUserPassword()));
        }
        if (user.getUserDetails() == null) {
            user.setUserDetails(new UserDetails());
        }
        user.getUserDetails().setName(userForUpdate.getUserDetails().getName());
        user.getUserDetails().setSurname(userForUpdate.getUserDetails().getSurname());
        user.getUserDetails().setPatronymic(userForUpdate.getUserDetails().getPatronymic());
        user.getUserDetails().setBirthDay(userForUpdate.getUserDetails().getBirthDay());
        userRepository.saveAndFlush(user);
        return user;
    }

    private String generateRandomString(Integer length) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }
}
