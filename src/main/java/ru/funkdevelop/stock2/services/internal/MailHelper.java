package ru.funkdevelop.stock2.services.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class MailHelper {
    @Autowired
    private JavaMailSender javaMailSender;

    @Async
    public void sendMail(String to, String subject, String text) throws MailException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(to);
        mail.setFrom("no-reply@funkdev.ru");
        mail.setSubject(subject);
        mail.setText(text);
        javaMailSender.send(mail);
    }

}