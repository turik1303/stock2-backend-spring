package ru.funkdevelop.stock2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.funkdevelop.stock2.data.DAO.interfaces.UserRepository;
import ru.funkdevelop.stock2.data.entities.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByUserLogin(login);

        if(user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", login));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        user.getUserRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRole())));


        return new org.springframework.security.core.userdetails.
                User(user.getUserLogin(), user.getUserPassword(), authorities);
    }
}
