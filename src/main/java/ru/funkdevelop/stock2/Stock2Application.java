package ru.funkdevelop.stock2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class Stock2Application {
    public static void main(String[] args) {

        SpringApplication.run(Stock2Application.class, args);
    }
}
