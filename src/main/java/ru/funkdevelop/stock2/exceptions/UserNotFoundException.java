package ru.funkdevelop.stock2.exceptions;

/**
 * Ошибка, выбрасываемая при отсутствии пользователя в базе
 */
public class UserNotFoundException extends Exception {
    private final String message;

    public UserNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
