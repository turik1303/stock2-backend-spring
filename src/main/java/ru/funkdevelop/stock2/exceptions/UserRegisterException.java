package ru.funkdevelop.stock2.exceptions;

/**
 * Исключение выбрасываемое при регистрации пользователя
 */
public class UserRegisterException extends Exception {
    private final String message;

    public UserRegisterException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
