package ru.funkdevelop.stock2.exceptions;

public class UserActivationNotFoundException extends Exception {
    private final String message;

    public UserActivationNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
