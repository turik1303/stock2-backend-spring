package ru.funkdevelop.stock2.exceptions;

public class CompanyNotFoundException extends Exception {
    private final String message;

    public CompanyNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
