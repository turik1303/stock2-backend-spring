package ru.funkdevelop.stock2.helpers;

public class EmailAddress {

    private String emailAddress;
    private String userName;

    public EmailAddress(String emailAddress, String userName) {
        this.emailAddress = emailAddress;
        this.userName = userName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
