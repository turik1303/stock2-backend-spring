package ru.funkdevelop.stock2.restControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.funkdevelop.stock2.controllers.UserService;
import ru.funkdevelop.stock2.data.DAO.interfaces.UserRepository;
import ru.funkdevelop.stock2.data.entities.User;
import ru.funkdevelop.stock2.services.internal.MailHelper;

@RestController
@RequestMapping("/springjwt")
public class ResourceController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MailHelper mailHelper;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/cities", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('admin') or hasAuthority('user')")
    public Object getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object currentPrincipalName = authentication.getPrincipal();
        System.out.println(currentPrincipalName);

        mailHelper.sendMail("turik1303@mail.ru", "test Java mail", "test text <h1>QQ</h1>");

        return currentPrincipalName;
    }

    @RequestMapping(value = "/register")
    public void registerUser() {
        User newUser = new User();
        newUser.setUserLogin("test");
        newUser.setUserPassword("11");
        newUser.setActive(false);
        newUser.setUserEmail("123@123.123");
        try {
            userService.register(newUser);
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}