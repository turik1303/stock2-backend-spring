package ru.funkdevelop.stock2.restControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.funkdevelop.stock2.controllers.UserService;
import ru.funkdevelop.stock2.data.entities.User;
import ru.funkdevelop.stock2.exceptions.UserActivationNotFoundException;
import ru.funkdevelop.stock2.exceptions.UserNotFoundException;
import ru.funkdevelop.stock2.exceptions.UserRegisterException;

import java.security.Principal;

@RestController
@RequestMapping("/user")
public class UserRestController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/checklogin/", method = RequestMethod.GET)
    public Boolean checkLogin(@RequestParam(value = "login") String login) {
        return userService.checkLogin(login);
    }

    @RequestMapping(value = "/checkemail/", method = RequestMethod.GET)
    public Boolean checkEmail(@RequestParam(value = "email") String email) {
        return userService.checkEmail(email);
    }

    @RequestMapping(value = "/refreshpassword/", method = RequestMethod.PUT)
    public Boolean refreshPassword(@RequestBody RefreshPasswordBody body) {
        try {
            return userService.refreshPassword(body.login, body.email);
        } catch (UserNotFoundException e) {
            System.out.println(e);
            return false;
        }
    }

    @RequestMapping(value = "/activate/", method = RequestMethod.PUT)
    @PreAuthorize(value = "isAuthenticated()")
    public Boolean activateUser(@RequestBody ActivateBody body) {
        try {
            return userService.activate(body.activationCode);
        } catch (UserActivationNotFoundException | UserNotFoundException e) {
            System.out.println(e);
            return false;
        }
    }

    @RequestMapping(value = "/register/", method = RequestMethod.POST)
    public User registerUser(@RequestBody User user) {
        try {
            return userService.register(user);
        } catch (UserRegisterException e) {
            System.out.println(e);
            return null;
        }
    }

    @RequestMapping(value = "/{id}/", method = RequestMethod.GET)
    @PreAuthorize(value = "isAuthenticated()")
    public User getUser(Long id) {
        try {
            User user = userService.getUser(id);
            user.setUserPassword("[hidden]");
            return user;
        } catch (UserNotFoundException e) {
            System.out.println(e);
            return null;
        }
    }

    @RequestMapping(value = "/byusername/{userName}/", method = RequestMethod.GET)
    @PreAuthorize(value = "isAuthenticated()")
    public User getUserByUserName(@PathVariable("userName") String userName) {
        try {
            User user = userService.getUserByUserName(userName);
            user.setUserPassword("[hidden]");
            return user;
        } catch (UserNotFoundException e) {
            System.out.println(e);
            return null;
        }
    }

    @RequestMapping(value = "/username/", method = RequestMethod.GET)
    public User currentUserName(Principal principal) {
        String userName = principal.getName();
        try {
            User user = userService.getUserByUserName(userName);
            user.setUserPassword("[hidden]");
            return user;
        } catch (UserNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/update/", method = RequestMethod.PUT)
    @PreAuthorize("isAuthenticated()")
    public User updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

    public static class ActivateBody {
        private String activationCode;

        public String getActivationCode() {
            return activationCode;
        }

        public void setActivationCode(String activationCode) {
            this.activationCode = activationCode;
        }
    }

    public static class RefreshPasswordBody {
        private String login;
        private String email;

        public RefreshPasswordBody() {
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
