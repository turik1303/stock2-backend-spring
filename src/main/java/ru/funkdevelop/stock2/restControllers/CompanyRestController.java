package ru.funkdevelop.stock2.restControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.funkdevelop.stock2.controllers.CompanyService;
import ru.funkdevelop.stock2.data.entities.Company;

import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyRestController {
    @Autowired
    private CompanyService companyService;
    @RequestMapping(value = "/all/", method = RequestMethod.GET)
    public List<Company> getAll() {
        List<Company> companies = companyService.getAllCompanies();
        return companies;
    }
}
